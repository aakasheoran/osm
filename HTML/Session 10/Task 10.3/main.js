let arr = window.prompt("Enter values");
let array = arr.split(",");

for (let i = 0; i < array.length; i++) {
  if (isNaN(array[i])) {
    array.splice(i, 1);
    i--;
  }
}

let len = array.length;
let maxInArray = Math.max(...array);
let temp = [];
temp.length = maxInArray + 1;
temp.fill(0);

for (let i = 0; i < len; i++) {
  temp[array[i]]++;
}

let indexOfMaxInTemp = temp.indexOf(Math.max(...temp));
let ValueOfMaxInTemp = temp[indexOfMaxInTemp];

let temp1 = temp.splice(indexOfMaxInTemp, 1);
let indexOfMaxInTemp2 = temp.indexOf(Math.max(...temp));

window.alert(
  "The number " +
    indexOfMaxInTemp +
    " appears " +
    (ValueOfMaxInTemp - temp[indexOfMaxInTemp2]) +
    " times more than any other element in array"
);

let a = 5;
a + 5;
let b = 10;
console.log(a + b);
