let string = window.prompt("Enter a string");

function charFreq(str) {
  let freq = {};
  for (let i = 0; i < str.length; i++) {
    let char = str.charAt(i);
    if (freq[char]) {
      freq[char]++;
    } else {
      freq[char] = 1;
    }
  }
  return freq;
}

let freq1 = charFreq(string);

console.log(freq1);
