let fname = window.prompt("First name");
fname = fname.toUpperCase();

let lname = window.prompt("Last name");
lname = lname.toUpperCase();

let gender = window.prompt("Gender (M or F)");
gender = gender.toUpperCase();

let dol = window.prompt("Date of license application (y-m-d)");
dol = new Date(dol);

let dob = window.prompt("Date of birth (y-m-d)");
dob = new Date(dob);

let initials = fname[0] + lname[0];
let years = dol.getFullYear() + dob.getFullYear();
let str1 = initials + years;

let f = fname.length;
let l = lname.length;
let lastTwo = fname[f - 1] + lname[l - 1];
let str2 = str1 + lastTwo;

let current = new Date();
let age = current.getFullYear() - dob.getFullYear();
let m = current.getMonth() - dob.getMonth();
let d = current.getDate() - dob.getDate();
if (m < 0 || (m === 0 && d < 0)) {
  age--;
}

alert("Your license number: \n" + str2 + age);
