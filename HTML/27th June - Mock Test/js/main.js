$(document).ready(function () {
  $("#make_list").change(function () {
    let maker = $("#make_list option:selected").text();
    // $("#choose_model").text("Model");
    if (maker === "Maruti") {
      $("#cars_list").html(
        "<label>Model</label><select><option>800 (1995)</option><option>Alto (2000)</option><option>WagonR (2002)</option><option>Esteem (2004)</option><option>SX4 (2007)</option></select>"
      );
    } else if (maker === "Tata") {
      $("#cars_list").html(
        "<label>Model</label><select><option>Indica (2001)</option><option>Indigo (2006)</option><option>Safari (2003)</option><option>Sumo (2001)</option></select>"
      );
    } else if (maker === "Chevorlet") {
      $("#cars_list").html(
        "<label>Model</label><select><option>Beat (2006)</option><option>Travera (2002)</option><option>Spark (2007)</option></select>"
      );
    } else if (maker === "Toyota") {
      $("#cars_list").html(
        "<label>Model</label><select><option>Camary (2005)</option><option>Etios (2010)</option><option>Corolla (2003)</option><option>Endeavour (2008)</option></select>"
      );
    }
  });

  $("form").submit(function () {
    $("#msg").text("You're car has been added to our garage for servicing.");
    $("#msg").css({ color: "#009900", "margin-bottom": "10px" });
  });
});
