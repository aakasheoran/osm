-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: mock_test
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author_details`
--

DROP TABLE IF EXISTS `author_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `author_details` (
  `id` int NOT NULL,
  `author_name` varchar(45) NOT NULL,
  `dob` date NOT NULL,
  `description` varchar(45) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author_details`
--

LOCK TABLES `author_details` WRITE;
/*!40000 ALTER TABLE `author_details` DISABLE KEYS */;
INSERT INTO `author_details` VALUES (1,'Chetan Bhagat','1974-04-22','A great writer','2020-06-20 11:08:08','2020-06-20 12:57:45','admin','admin',1),(2,'Ravinder Sharma','1982-02-04','Very good author','2020-06-20 11:15:16',NULL,'admin',NULL,1),(3,'J K Rowling','1965-07-31','Best selling author','2020-06-20 11:17:43',NULL,'admin',NULL,1);
/*!40000 ALTER TABLE `author_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_details`
--

DROP TABLE IF EXISTS `book_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book_details` (
  `id` int NOT NULL,
  `book_title` varchar(45) NOT NULL,
  `author_id` int DEFAULT NULL,
  `publisher_id` int DEFAULT NULL,
  `book_genre` varchar(45) NOT NULL,
  `release_year` int NOT NULL,
  `rating` decimal(2,1) NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `author_id` (`author_id`),
  KEY `publisher_id` (`publisher_id`),
  CONSTRAINT `book_details_ibfk_1` FOREIGN KEY (`author_id`) REFERENCES `author_details` (`id`),
  CONSTRAINT `book_details_ibfk_2` FOREIGN KEY (`publisher_id`) REFERENCES `publisher_details` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_details`
--

LOCK TABLES `book_details` WRITE;
/*!40000 ALTER TABLE `book_details` DISABLE KEYS */;
INSERT INTO `book_details` VALUES (1,'The Girl In Room 105',1,1,'Fiction, Romance, Mystery, Thriller, Suspense',2018,4.3,'2020-06-20 11:40:22',NULL,'admin',NULL,1),(2,'Five Point Someone',1,1,'Humor, Fiction',2004,3.4,'2020-06-20 11:44:41',NULL,'admin',NULL,1),(3,'Your Dreams Are Mine Now',2,3,'Fiction, Romance novel, Contemporary romance',2014,4.3,'2020-06-20 11:54:33',NULL,'admin',NULL,1),(4,'Will You Still Love Me?',2,3,'Fiction, Romance novel, Contemporary romance',2018,4.5,'2020-06-20 11:56:41',NULL,'admin',NULL,1),(5,'Harry Potter And The Deathly Hallows',3,2,'Mystery, Thriller, Fantasy Fiction',2007,4.8,'2020-06-20 11:57:59',NULL,'admin',NULL,1),(6,'Fantastic Beasts and Where to Find Them',3,2,'Drama, Fantasy Fiction, Adventure fiction',2001,4.0,'2020-06-20 12:03:55',NULL,'admin',NULL,1);
/*!40000 ALTER TABLE `book_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_like`
--

DROP TABLE IF EXISTS `book_like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book_like` (
  `id` int NOT NULL,
  `book_id` int NOT NULL,
  `liked_by` int NOT NULL,
  `liked_on` datetime NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_id` (`book_id`),
  KEY `liked_by` (`liked_by`),
  CONSTRAINT `book_like_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book_details` (`id`),
  CONSTRAINT `book_like_ibfk_2` FOREIGN KEY (`liked_by`) REFERENCES `user_reg` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_like`
--

LOCK TABLES `book_like` WRITE;
/*!40000 ALTER TABLE `book_like` DISABLE KEYS */;
INSERT INTO `book_like` VALUES (1,1,4,'2020-03-16 08:23:19','2020-06-20 12:33:44',NULL,'admin',NULL,1),(2,6,3,'2020-04-11 07:13:14','2020-06-20 12:34:38',NULL,'admin',NULL,1),(3,4,1,'2020-03-22 11:03:21','2020-06-20 12:34:56',NULL,'admin',NULL,1),(4,5,2,'2020-06-26 01:23:12','2020-06-20 12:35:16',NULL,'admin',NULL,1),(5,2,5,'2020-01-01 12:46:37','2020-06-20 12:36:31',NULL,'admin',NULL,1);
/*!40000 ALTER TABLE `book_like` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_rent`
--

DROP TABLE IF EXISTS `book_rent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book_rent` (
  `id` int NOT NULL,
  `book_id` int NOT NULL,
  `rented_by` int NOT NULL,
  `rented_on` datetime NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_id` (`book_id`),
  KEY `rented_by` (`rented_by`),
  CONSTRAINT `book_rent_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book_details` (`id`),
  CONSTRAINT `book_rent_ibfk_2` FOREIGN KEY (`rented_by`) REFERENCES `user_reg` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_rent`
--

LOCK TABLES `book_rent` WRITE;
/*!40000 ALTER TABLE `book_rent` DISABLE KEYS */;
INSERT INTO `book_rent` VALUES (1,2,3,'2020-06-02 11:01:33','2020-06-20 01:12:14',NULL,'admin',NULL,1),(2,3,1,'2020-04-24 12:33:27','2020-06-20 01:13:24',NULL,'admin',NULL,1),(3,1,5,'2020-05-16 09:54:41','2020-06-20 01:14:13',NULL,'admin',NULL,1),(4,5,4,'2020-03-15 08:07:52','2020-06-20 01:15:52',NULL,'admin',NULL,1),(5,4,2,'2020-02-05 03:21:43','2020-06-20 01:17:32',NULL,'admin',NULL,1);
/*!40000 ALTER TABLE `book_rent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_wishlist`
--

DROP TABLE IF EXISTS `book_wishlist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book_wishlist` (
  `id` int NOT NULL,
  `book_id` int NOT NULL,
  `wishlisted_by` int NOT NULL,
  `wishlisted_on` datetime NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_id` (`book_id`),
  KEY `wishlisted_by` (`wishlisted_by`),
  CONSTRAINT `book_wishlist_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book_details` (`id`),
  CONSTRAINT `book_wishlist_ibfk_2` FOREIGN KEY (`wishlisted_by`) REFERENCES `user_reg` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_wishlist`
--

LOCK TABLES `book_wishlist` WRITE;
/*!40000 ALTER TABLE `book_wishlist` DISABLE KEYS */;
INSERT INTO `book_wishlist` VALUES (1,3,1,'2020-01-16 01:15:23','2020-06-20 01:34:41',NULL,'admin',NULL,1),(2,1,4,'2020-05-30 03:19:14','2020-06-20 01:35:42',NULL,'admin',NULL,1),(3,5,2,'2020-04-21 11:37:29','2020-06-20 01:36:11',NULL,'admin',NULL,1),(4,2,5,'2020-03-25 04:17:31','2020-06-20 01:37:31',NULL,'admin',NULL,1),(5,6,3,'2020-02-26 07:40:45','2020-06-20 01:23:22',NULL,'admin',NULL,1);
/*!40000 ALTER TABLE `book_wishlist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `friends` (
  `id` int NOT NULL,
  `user1_id` int NOT NULL,
  `user2_id` int NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
INSERT INTO `friends` VALUES (1,1,4,'2020-06-20 12:15:13',NULL,'admin',NULL,1),(2,2,3,'2020-06-20 12:16:33',NULL,'admin',NULL,1);
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publisher_details`
--

DROP TABLE IF EXISTS `publisher_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `publisher_details` (
  `id` int NOT NULL,
  `publisher_name` varchar(45) NOT NULL,
  `established_on` date NOT NULL,
  `address` varchar(100) NOT NULL,
  `description` varchar(45) NOT NULL,
  `created_on` varchar(45) NOT NULL,
  `modified_on` varchar(45) DEFAULT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher_details`
--

LOCK TABLES `publisher_details` WRITE;
/*!40000 ALTER TABLE `publisher_details` DISABLE KEYS */;
INSERT INTO `publisher_details` VALUES (1,'Rupa Publications','1936-01-01','Kolkata, Inida','Published all books of Chetan Bhagat','2020-06-20 11:20:24',NULL,'admin',NULL,1),(2,'Hachette Book Group','2006-03-31','New York City, USA','Publisher of J.K. Rowling','2020-06-20 11:22:37',NULL,'admin',NULL,1),(3,'Srishti Publications','2009-04-05','New Delhi, India','Published Ravinder Singhs\'s novels','2020-06-20 11:36:12',NULL,'admin',NULL,1);
/*!40000 ALTER TABLE `publisher_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_reg`
--

DROP TABLE IF EXISTS `user_reg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_reg` (
  `id` int NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `gender` enum('M','F') NOT NULL,
  `dob` date NOT NULL,
  `user_type` varchar(10) NOT NULL,
  `address` varchar(100) NOT NULL,
  `phone_number` bigint NOT NULL,
  `created_on` datetime NOT NULL,
  `modified_on` datetime DEFAULT NULL,
  `created_by` varchar(45) NOT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_reg`
--

LOCK TABLES `user_reg` WRITE;
/*!40000 ALTER TABLE `user_reg` DISABLE KEYS */;
INSERT INTO `user_reg` VALUES (1,'Aakash','Sheoran','M','1997-11-25','Premium','H No 45, Street ABC, Area XYZ,  State JKL, AAA',9856325497,'2020-06-20 11:59:23',NULL,'admin',NULL,1),(2,'Mahesh','Chandra','M','1996-06-13','Normal','H No 12, Street ACD, Area XXY,  State LMN, AAA',9758463258,'2020-06-20 12:32:12',NULL,'admin',NULL,1),(3,'Somesh','Yadav','M','1995-03-17','Normal','H No 36, Street BCD, Area YYY,  State LLL, AAA',9485679589,'2020-06-20 12:33:45',NULL,'admin',NULL,1),(4,'Priya','Sharma','F','1997-09-04','Premium','H No 04, Street LOP, Area PQR,  State HJK, AAA',9685964785,'2020-06-20 12:35:13',NULL,'admin',NULL,1),(5,'Riya','Sheoran','F','2000-06-30','Premium','H No 14, Street GJK, Area YUP,  State FGH, AAA',9786123459,'2020-06-20 12:36:43',NULL,'admin',NULL,1);
/*!40000 ALTER TABLE `user_reg` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-20 13:53:43
