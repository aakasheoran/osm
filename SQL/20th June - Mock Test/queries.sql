SELECT first_name, last_name, user_type FROM mock_test.user_reg WHERE user_type = "Normal";

SELECT * FROM mock_test.user_reg WHERE gender = "F";

SELECT book_genre AS all_genres FROM mock_test.book_details; 

SELECT id, book_title FROM mock_test.book_details WHERE rating > 4;

SELECT book_title AS highest_rated, MAX(rating) AS rating FROM mock_test.book_details; 

SELECT book_title AS lowest_rated, MIN(rating) AS rating FROM mock_test.book_details;

SELECT publisher_name FROM mock_test.publisher_details WHERE YEAR(established_on) < 2012;

SELECT author_name FROM mock_test.author_details WHERE author_name LIKE 'AR%';

SELECT book_title, rating FROM mock_test.book_details ORDER BY rating DESC LIMIT 5;

SELECT book_title FROM mock_test.book_details WHERE release_year >= 2012 AND release_year <= 2018;


